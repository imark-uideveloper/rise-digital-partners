var particleSphere = (function (window, document, $) {
    var camera, cameraTarget, cameraDummy;
    var mouse;

    var scene, renderer;

    var angle = Math.PI * 2 / 10;
    var rotation = 0,
        rotationTarget = 0;
    var movementX, movementX;
    var geometry1, geometry2, geometry3, line1, line2, line3;

    /* GUI VARS */
    var particles = [];

    var cameraVars = {
        fov: 70,
        near: 100,
        far: 5000,
        xRange: 10,
        yRange: 10
    };
    var particleVars = {
        amount: 1500,
        dispersion: 800,
        smallestSize: 5,
        largestSize: 15,
        rangeXNear: 2000,
        rangeXFar: 4000,
        rangeYNear: 2000,
        rangeYFar: 4000,
        rangeZNear: 2000,
        rangeZFar: 4000,
        lifeSpan: 1000,
        particleColor: [135, 244, 231, 0.1],
        updateColor: function () {
            onUpdateParticles();
        }
    };

    var lineVars = {
        lineEveryXParticles: 300,
        line1Color: [255, 255, 255, 0.1],
        line2Color: [255, 255, 255, 0.1],
        line3Color: [255, 255, 255, 0.1],
        line1Opacity: 0.2,
        line2Opacity: 0.2,
        line3Opacity: 0.2,
        updateLineLength: function () {
            onUpdateLineLength();
        }
    };

    //LOADER
    var loader, loaderMesh;

    self.init = function () {
        WINDOW_WIDTH = window.innerWidth;
        WINDOW_HEIGHT = window.innerHeight;

        camera = new THREE.PerspectiveCamera(
            cameraVars.fov,
            WINDOW_WIDTH / WINDOW_HEIGHT,
            cameraVars.near,
            cameraVars.far
        );

        cameraTarget = new THREE.Vector3(0, 0, -500);
        cameraDummy = new THREE.Object3D();
        cameraDummy.position.set(Math.sin(0) * 500, 0, Math.cos(0) * 500);
        cameraDummy.add(camera);

        scene = new THREE.Scene();
        scene.add(cameraDummy);

        mouse = new THREE.Vector2();

        var light = new THREE.DirectionalLight(0xffffff, 1.2);
        light.position.set(1, 1, 1);
        scene.add(light);

        var light = new THREE.DirectionalLight(0xffffff, 0.75);
        light.position.set(-1, -0.5, -1);
        scene.add(light);

        /* CLOUD */
        var PI2 = Math.PI * 2;
        var particleColor = setColor(particleVars.particleColor);
        var material = new THREE.ParticleCanvasMaterial({
            color: particleColor,
            program: function (context) {
                context.beginPath();
                context.arc(0, 0, 0.2, 0, PI2, true);
                context.closePath();
                context.fill();
            }
        });

        geometry1 = new THREE.Geometry();
        geometry2 = new THREE.Geometry();
        geometry3 = new THREE.Geometry();

        for (var i = 0; i < particleVars.amount; i++) {
            particle = new THREE.Particle(material);

            var sphereCoords = generateSphericalPosition();

            particle.position.x = sphereCoords.x;
            particle.position.y = sphereCoords.y;
            particle.position.z = sphereCoords.z;
            particle.position.normalize();
            particle.position.multiplyScalar(
                Math.random() * 10 + particleVars.dispersion
            );

            particle.originX = particle.position.x;
            particle.originY = particle.position.y;
            particle.originZ = particle.position.z;

            if (i % lineVars.lineEveryXParticles == 0) {
                geometry1.vertices.push(particle.position);
            } else if (i % lineVars.lineEveryXParticles == 1) {
                geometry2.vertices.push(particle.position);
            } else if (i % lineVars.lineEveryXParticles == 3) {
                geometry3.vertices.push(particle.position);
            }

            initParticle(particle, i);
            particles.push(particle);
            scene.add(particle);
        }

        var line1Color = setColor(lineVars.line1Color);
        var line2Color = setColor(lineVars.line2Color);
        var line3Color = setColor(lineVars.line3Color);

        line1 = new THREE.Line(
            geometry1,
            new THREE.LineBasicMaterial({
                color: line1Color,
                opacity: lineVars.line1Opacity
            })
        );
        line2 = new THREE.Line(
            geometry2,
            new THREE.LineBasicMaterial({
                color: line2Color,
                opacity: lineVars.line2Opacity
            })
        );
        line3 = new THREE.Line(
            geometry3,
            new THREE.LineBasicMaterial({
                color: line3Color,
                opacity: lineVars.line3Opacity
            })
        );
        scene.add(line1);
        scene.add(line2);
        scene.add(line3);

        renderer = new THREE.CanvasRenderer();

        renderer.setSize(window.innerWidth, window.innerHeight);
        //        renderer.domElement.style.backgroundColor = "#000";

        render();
    };

    function render() {
        document.getElementById("animation").appendChild(renderer.domElement);
        animate();
    }

    function animate() {
        requestAnimationFrame(animate);
        TWEEN.update();

        var x = mouse.x * 100.0;
        var y = mouse.y * 100.0;

        camera.position.x += (x - camera.position.x) * 0.1;
        camera.position.y += (y - camera.position.y) * 0.1;
        camera.lookAt(cameraTarget);

        rotation += (rotationTarget - rotation) * 0.1;

        cameraDummy.position.x = Math.sin(rotation * angle) * 1500;
        cameraDummy.position.z = Math.cos(rotation * angle) * 1500;
        cameraDummy.rotation.y = rotation * angle;

        renderer.render(scene, camera);
    }

    function initParticle(particle, index) {
        var particle = this instanceof THREE.Particle ? this : particle;
        var delay = delay !== undefined ? delay : 0;

        generateParticle(
            particle,
            particleVars.smallestSize,
            particleVars.largestSize
        );

        explode(particle, index);
    }

    function setColor(colorArray) {
        var newColor =
            "rgb(" + colorArray[0] + "," + colorArray[1] + "," + colorArray[2] + ")";
        return newColor;
    }

    function generateParticle(particle, min, max) {
        particle.scale.x = particle.scale.y = Math.random() * max + min;
    }

    function generateSphericalPosition() {
        var sphereCoords = {};
        sphereCoords.x = Math.random() * 2 - 1;
        sphereCoords.y = Math.random() * 2 - 1;
        sphereCoords.z = Math.random() * 2 - 1;

        return sphereCoords;
    }

    function explode(particle, index) {
        particle.particleTween = new TWEEN.Tween(particle.position)
            .delay(index)
            .to({
                    x: Math.random() * particleVars.rangeXFar - particleVars.rangeXNear,
                    y: Math.random() * particleVars.rangeYFar - particleVars.rangeYNear,
                    z: Math.random() * particleVars.rangeZFar - particleVars.rangeZNear
                },
                10000
            )
            .onComplete(function () {
                (particle.position.x = particle.originX),
                (particle.position.y = particle.originY),
                (particle.position.z = particle.originZ),
                explode(particle, index);
            })
            .start();
    }

    function implode(particle) {
        particle.particleTween = new TWEEN.Tween(particle.position)
            .delay(2000)
            .to({
                    x: particle.originX,
                    y: particle.originY,
                    z: particle.originZ
                },
                Math.random() * 2000
            )
            .onComplete(function () {
                explode(particle);
            })
            .start();
    }

    function onUpdateCamera() {
        camera.updateProjectionMatrix();
    }

    function onUpdateLines() {
        var line1Color = updateColor(lineVars.line1Color);
        line1.material.color.r = line1Color.r;
        line1.material.color.g = line1Color.g;
        line1.material.color.b = line1Color.b;

        var line1Color = updateColor(lineVars.line1Color);
        line1.material.color.r = line1Color.r;
        line1.material.color.g = line1Color.g;
        line1.material.color.b = line1Color.b;

        var line2Color = updateColor(lineVars.line2Color);
        line2.material.color.r = line2Color.r;
        line2.material.color.g = line2Color.g;
        line2.material.color.b = line2Color.b;

        var line3Color = updateColor(lineVars.line3Color);
        line3.material.color.r = line3Color.r;
        line3.material.color.g = line3Color.g;
        line3.material.color.b = line3Color.b;
    }

    var onDocumentMouseDown = function (event) {
        onDocumentMouseDownX = event.clientX;

        var onDocumentMouseMove = function (event) {
            document.body.style.cursor = "move";

            var movementX =
                event.movementX || event.mozMovementX || event.webkitMovementX || 0;

            rotationTarget -= movementX * 0.001;
        };

        var onDocumentMouseUp = function (event) {
            document.body.style.cursor = "pointer";

            document.removeEventListener("mousemove", onDocumentMouseMove);
            document.removeEventListener("mouseup", onDocumentMouseUp);
        };

        document.addEventListener("mousemove", onDocumentMouseMove, false);
        document.addEventListener("mouseup", onDocumentMouseUp, false);
    };

    var onDocumentMouseMove = function (event) {
        event.preventDefault();
        var percentX = event.clientX / window.innerWidth;
        var percentY = event.clientY / window.innerHeight;

        mouse.x = percentX * cameraVars.xRange - cameraVars.xRange / 2;
        mouse.y = percentY * cameraVars.yRange - cameraVars.yRange / 2;
    };

    return {
        init: init
    };
})(window, document, jQuery);

window.onload = function () {
    particleSphere.init();
};
