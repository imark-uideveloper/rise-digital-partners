$(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();

    //Sticky Header
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            $(".header-wrapper").addClass("fixed-header");
        } else {
            $(".header-wrapper").removeClass("fixed-header");
        }
    });

    //Toggle Header
    $(".collapsed-button").on('click', function () {
        if ($(this).hasClass('closed')) {
            $('.collapsed-button').removeClass('closed');
            $('#header-wrapper').removeClass('open');
            $('body').removeClass('scroll-disable');
            $('.header-wrapper').removeClass('slide');
        } else {
            $(this).addClass('closed');
            $('#header-wrapper').addClass('open');
            $('body').addClass('scroll-disable');
            $('.header-wrapper').addClass('slide');
        }
    });


    $('.collapsed-button').click(function () {
        $('#sidebar-header-wrapper').toggleClass('slide-in');
    });

    //Slick Slider

    /*----- Business Slider ------*/

    $('.business-slider').slick({
        dots: true,
        infinite: true,
        speed: 800,
        autoPlay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        prevArrow: '<a class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>',
        nextArrow: '<a class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false
                }
            }
        ]
    });

    /*----- Special Slider -----*/

    $('.special-slider').slick({
        dots: true,
        infinite: true,
        speed: 800,
        autoPlay: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 668,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });

    //Page Loading Animation

    jQuery('.post').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInDown', // Class to add to the elements when they are visible
        offset: 100
    });
});

//Textarea 

$(function (jQuery) {
    $('.firstCap').on('keypress', function (event) {
        var $this = $(this),
            thisVal = $this.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        $(this).val(FLC + con);
    });
});

jQuery(document).ready(function ($) {
    jQuery('.what-we-do-list a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery("nav li a").removeClass("active");
                jQuery(this).addClass('active');
                jQuery('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

// ===== Scroll to Top ==== 
$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200); // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200); // Else fade out the arrow
    }
});
$('#return-to-top').click(function () { // When arrow is clicked
    $('body,html').animate({
        scrollTop: 0 // Scroll to top of body
    }, 500);
});
